package Materia;





public class Materia {
    
    private String nombre;
    private double talleres;
    private double examenes;
    private double notafinal;
    private String concepto;

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String n) {
        this.nombre = nombre;    
    }
    public double getTalleres() {
        return talleres;
    }
    public void setcalcularTalleres(double t1, double t2, double t3) {
      this.talleres = ((t1+t2+t3)/3)*0.4;
    }
    public double getExamenes() {
        return examenes;
    }
    public void setExamenes(double e1, double e2) {
        this.examenes = ((e1+e2)/2)*0.6;
    }
    public double getNotafinal() {
        return notafinal;
    }
    public void setNotafinal(double E, double T) {
        this.notafinal=(E+T);
    }
    public String getConcepto() {
  
         if(this.notafinal<3.5){
            return "reprobo";
        }
        else{
            return "Aprobo";}
         
    }
 
    
    public Materia(String n){
    this.nombre=n;
    this.talleres=0;
    this.examenes=0;
    this.notafinal=0;
   // this.concepto="";
    }
        public static void main(String[] args) {
     
           Materia a = new Materia(" andres "+"felipe\n"+" matematicas");
            System.out.println(" Su nombre es: "+a.getNombre());
            a.setcalcularTalleres(5, 4, 3);
            System.out.println(" Promedio de talleres es: "+a.getTalleres());
            a.setExamenes(4.0, 2.5);
            System.out.println(" Su promedio en examenes es: "+a.getExamenes());
            a.setNotafinal(a.getTalleres(),a.getExamenes());
            System.out.println(" Nota final: "+a.getNotafinal());
            
            System.out.println(" concepto: " +a.getConcepto());       
          
    }
    
}
